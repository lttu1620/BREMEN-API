<?php

namespace Fuel\Tasks;

use Fuel\Core\Cli;
use Fuel\Core\Config;

/**
 * Push message to device
 *
 * @package             Tasks
 * @create              2015-07-27
 * @version             1.0
 * @author              thailh
 * @run                 php oil refine pushmessage
 * @run                 FUEL_ENV=test php oil refine pushmessage
 * @run                 FUEL_ENV=production php oil refine pushmessage
 * @copyright           Oceanize INC
 */
class PushMessage {

    public static function run() {
        \Package::load('gcm');   
        \Package::load('apns'); 
        \LogLib::info('BEGIN [Push message] ' . date('Y-m-d H:i:s'), __METHOD__, array());
        Cli::write("BEGIN [Push message] ".date('Y-m-d H:i:s')." PROCESSING \n\n . . . . ! \n");
        $messages = \Model_Push_Message::get_for_task();
        if (empty($messages)) {
            Cli::write('There are no message to sent');
            return false;
        }
        foreach ($messages as $message) {
            foreach ($message->user_notifications as $user_notification) {
                $apple_regid = $user_notification->get('apple_regid');
                $google_regid = $user_notification->get('google_regid');
                $is_sent = 0;
                if (!empty($google_regid)) {
                    Cli::write('Send message to Android device ' . $google_regid);
                    if (!\Gcm::sendMessage(array(
                        'google_regid' => $google_regid,
                        'message' => $message->get('message')
                    ))) {
                        \LogLib::error('Can not send message to Android device ', __METHOD__, $google_regid);
                        return false;   
                    }
                    $is_sent = 1;
                }
                if (!empty($apple_regid)) {
                    Cli::write('Send message to iOS device ' . $apple_regid);
                    if (!\Apns::sendMessage(array(
                        'apple_regid' => $apple_regid,
                        'message' => json_decode($message->get('message'))->notice,                
                    ))) {
                        \LogLib::error('Can not send message to iOS device ', __METHOD__, $apple_regid);
                        return false;   
                    }
                    $is_sent = 1;
                }
                if ($is_sent) {               
                    $message->set('is_sent', '1');
                    $message->set('sent_date', time());
                    $message->save();
                }
            }
        }
        \LogLib::info('END [Push message] ' . date('Y-m-d H:i:s'), __METHOD__, array());
        Cli::write("END [Push message] ".date('Y-m-d H:i:s')."\n");
        exit;
    }

}
