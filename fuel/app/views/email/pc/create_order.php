<?php 
echo "
{$user_name}様<br>
<br>
FASTNAILへのご予約内容を受け付けました。ありがとうございます。<br>
<br>
下記の予約内容をご確認いただき、そのままご予約の日時にご来店ください。<br>
※本メールは配信専用のため、ご返信いただきましてもサロンへは届きません。<br>
<br>
■予約番号<br>
    {$order_id}<br>
<br>
■来店日時<br>
    {$reservation_date}<br>
<br>
■店舗名<br>
    {$shop_name}<br>
<br>
■指名スタッフ<br>
    {$nailist_name}<br>
<br>
■住所<br>
    {$shop_address}<br>
<br>
■地図<br>
    {$shop_map_url}<br>
<br>
■電話番号<br>
    {$shop_phone}<br>
<br>
■メニュー<br>
    {$order_service}<br>
<br>
■合計金額<br>
    予約時合計金額　".number_format($total_price)."円(税込　".number_format($total_price+$total_tax_price)."円)<br>
<br>
■キャンセルに関して<br>
 キャンセルされる場合は、直接サロンへご連絡ください。<br>
<br>
※指名につきましては、ファストネイル プラスでのみ承っております<br>
<br>
※ご連絡のないキャンセルをされた場合、ご予約を承れなくなることがあります。<br>
　 <br>
※本メールはお客様にご入力いただいたメールアドレス宛に発信しているため、<br>
入力ミスなどの理由によりまったく別の方にメールが届く可能性があります。<br>
もし本メールにお心当たりが無い場合は、<br>
お手数ですが、破棄していただけますようお願いします。<br>
<br>
※本メールにご返信いただきましても、対応いたしかねます。<br>
お問い合わせは、お手数ですがファストネイル {$shop_name}へ直接お電話いただけますようお願いいたします。<br>
<br>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━<br>
◆FASTNAIL オフィシャルサイト◆<br>
  ◇スマホ<br>
    http://s.fastnail.jp<br>
  ◇モバイル<br>
    http://fnail.jp<br>
  ◇パソコン<br>
    http://www.fastnail.jp<br>
  ◇予約システム<br>
    https://fastnail.town/<br>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━<br>";

