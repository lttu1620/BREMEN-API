<?php

namespace Bus;

/**
 * <Colors_Detail - API to get detail of Colors>
 *
 * @package Bus
 * @created 2014-11-21
 * @version 1.0
 * @author VuLTH
 * @copyright Oceanize INC
 */
class Reports_General extends BusAbstract
{
    /**
     * Call function operateDB() from model Color
     *
     * @author VuLTH
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try
        {
           
            $this->_response = \Model_Order::get_general($data);
            
            return $this->result(\Model_Order::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
