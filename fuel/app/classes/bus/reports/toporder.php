<?php

namespace Bus;

/**
 * Get top nail best-selling (using array count)
 *
 * @package Bus
 * @created 2015-03-24
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Reports_TopOrder extends BusAbstract
{
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'page',
        'limit'
    );

    /** @var array $_default_value field default */
    protected $_default_value = array(
        'page'  => '1',
        'limit' => '10'
    );

    /**
     * Call function top_order() from model Shop
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Nail::top_order($data);
            return $this->result(\Model_Nail::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
