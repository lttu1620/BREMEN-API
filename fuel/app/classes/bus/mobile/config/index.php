<?php

namespace Bus;

/**
 * Get config for mobile
 *
 * @package Bus
 * @created 2015-06-16
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Mobile_Config_Index extends BusAbstract
{
    /**
     * Get config to mobile
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $config = array(
                'order_off_nails_price'       => array_merge(array(0 => '---'), \Config::get('order_off_nails_price')),
                'order_off_nails'             => array_merge(array(0 => '---'), \Config::get('order_off_nails')),
                'order_nail_type'             => array_merge(array(0 => '---'), \Config::get('order_nail_type')),
                'order_nail_length'           => array_merge(array(0 => '---'), \Config::get('order_nail_length')),
                'order_nail_add_length'       => array_merge(array(0 => '---'),\Config::get('order_nail_add_length')),
                'order_nail_add_length_price' => array_merge(array(0 => '---'),\Config::get('order_nail_add_length_price')),
                'order_service'               => array_merge(array(0 => '---'),\Config::get('order_service')),
                'user_visit_element'          => array_merge(array(0 => '---'),\Config::get('user_visit_element')),
            );            
            $this->_response = $config;
            return true;
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
