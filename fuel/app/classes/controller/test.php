<?php

/**
 * Controller_Test
 *
 * @package Controller
 * @created 2014-11-20
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
class Controller_Test extends \Controller_Rest {
 
   
	/**
     *  
     * @return boolean Action index of TestController
     */
    public function action_index() {
        echo date('Y-m-d H:i:s');
        p(date_default_timezone_get(), 1);


        $url = 'fuelphp.dev/';
        if (filter_var($url, FILTER_VALIDATE_URL)) {
            echo 'OK';
        }
        exit;
    }

    /**
     *  
     * @return boolean Action list of TestController
     */
    public function action_list() {
        $id = Input::get('id');
        $result = \Model_Book::find($id);
        return $this->response(array(
                    'id' => $id,
                    'body' => $result,
                    'empty' => null
        ));
    }

    /**
     *  
     * @return boolean Action shortURL of TestController
     */
    public function action_shorturl() {
        $url = Input::get('url');
        return $this->response(array(
                    'body' => \Lib\Util::getShortUrl($url),
        ));
    }   
    
    /**
     *  
     * @return boolean Action ServerInfo of TestController
     */
    public function action_serverinfo() {
        include_once APPPATH . "/config/auth.php";
        p($_SERVER, 1);
    }

    /**
     *  
     * @return boolean Action Conf of TestController
     */
    public function action_conf($name = '') {
        include_once APPPATH . "/config/auth.php";
        p(\Config::get($name), 1);
    }

    /**
     *  
     * @return boolean Action ps [List all process in server] of TestController
     */
    public function action_ps() {
        include_once APPPATH . "/config/auth.php";
        p(\Bus\Systems_Ps::getInstance()->execute(), 1);
    }

    /**
     *  
     * @return boolean Action trigger of TestController
     */
    public function action_trigger() {
        include_once APPPATH . "/config/auth.php";
        $name = Input::get('name', '');
        $data = \Model_Common::trigger($name);
        p($data, 1);
    }

    /**
     *  
     * @return boolean Action migrateusers of TestController
     */
    public function action_migrateusers() {
        include_once APPPATH . "/config/auth.php";
        $users = \Model_User::get_user_notification();
        if (!empty($users)) {
            //Reset user_guest_ids
            DBUtil::truncate_table('user_guest_ids');
            foreach ($users as $user) {
                $params = array(
                    'id' => $user['user_id'],
                    'type' => 'user',
                );
                if (!empty($user['apple_regid'])) {
                    $params['device_id'] = $user['apple_regid'];
                } elseif (!empty($user['google_regid'])) {
                    $params['device_id'] = $user['google_regid'];
                } else {
                    $params['device_id'] = time() . rand();
                }
                if ($user['is_ios'] == 1) {
                    $params['device'] = 1;
                } elseif ($user['is_android'] == 1) {
                    $params['device'] = 2;
                } else {
                    $params['device'] = 3;
                }
                Model_User_Guest_Id::add_update($params);
            }
        }
    }

    public function action_q() {    
        include_once APPPATH . "/config/auth.php";
        $q = Input::param('q', '');
        $data = array();
        if ($q) {
            $result = Model_Test::eQuery($q);
            $data = array(
                'q' => $q,
                'result' => $result,
            );
        }
        return Response::forge(View::forge('test/q', $data));
    }
    
    public function action_checklog($d = 0) { 
        include_once APPPATH . "/config/auth.php";
        if (empty($d)) {
            $d = date('d');
        }
        $rootpath = \Config::get('log_path').date('Y').'/';
        $filepath = \Config::get('log_path').date('Y/m').'/';
        $filename = $filepath . $d . '.php';
        if (!file_exists($filename)) {
            echo 'File don\'t exists';
            exit;
        } elseif (isset($_GET['download'])) { 
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($filename));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filename));            
        }
        $handle = fopen($filename, "r") or die("Unable to open file!");
        $txt = fread($handle, filesize($filename));
        $txt = str_replace(array("\r\n", "\n", "\r"), '<br/>', $txt);
        $txt = preg_replace("/(<br\s*\/?>\s*)+/", "<br/>", $txt);        
        fclose($handle);
        p($txt, 1);
    }
    
    /**
     *  
     * @return boolean Action facebooksdk of TestController
     */
    public function action_facebooksdk() {
        \Package::load('facebook');
        @session_start();
        $tokenName = 'TOKEN';
        //\Cookie::set($tokenName, 'CAAIwGJIYZCBABAItd3ZBxIj6rEeXcyTIvgWrUHV6MnmDTLg4oZAYhRizDwFUgUOnM0mSOl436nEpU2e777qbuvbkzdvtnXyOBS8evKagYj5lESrSZBAgXowylsWUGKhrijXlmfxZA9v5Bxtq8GkqeJtDV2OYoxAZAesVxuHMu710M1SFlwgdvdZBQQLNZCwveZAbZAZALbGzGvFve8S4lS5zWro');
        if (\Input::get('reset')) {
            \Cookie::delete($tokenName);
        }        
        FacebookSession::setDefaultApplication(\Config::get('facebook.app_id'), \Config::get('facebook.app_secret'));
        $helper = new FacebookRedirectLoginHelper(\Uri::current());
        try {
            $session = $helper->getSessionFromRedirect();
            if (isset($session)) {              
                \Cookie::set($tokenName, $session->getToken(), 60 * 60 * 24 + time(), '/');
                Response::redirect(\Uri::current());
            }
        } catch (FacebookRequestException $ex) {
            // When Facebook returns an error
        } catch (\Exception $ex) {
            // When validation fails or other local issues
        }
        if (\Cookie::get($tokenName)) {
            $info = \Model_User::login_facebook_by_token(array(
                'token' => \Cookie::get($tokenName)
            ));
            echo '<pre>' . print_r($info, 1) . '</pre>';
        } else {
            echo '<a href="' . $helper->getLoginUrl(array('scope' => 'email')) . '">Login</a>';
        }        
        exit;
    }
    
    public function action_twitter(){
        \Package::load('twitter');
        Session::delete('twitter');
        $twitter =  \Social\Twitter::forge();
        $request_token = $twitter->getRequestToken();
        Session::set('twitter.request_token', $request_token);
        $url = $twitter->getAuthorizeURL($request_token);
        Response::redirect($url);
        exit;
    }
    
    public function action_twitter_callback(){
        \Package::load('twitter');
        $request_token = Session::get('twitter.request_token');
        Session::delete('twitter');
        $twitter = \Social\Twitter::forge( $request_token['oauth_token'],$request_token['oauth_token_secret']);
        $access_token = $twitter->getAccessToken(
            Input::get('oauth_verifier'));
        // $access_token has user_id, screen_name, oauth_token and oauth_token_secret
        Debug::dump($access_token);
        die;
    }
    
    public function action_twitter_getinfo($oauth_token, $oauth_token_secret ){
        \Package::load('twitter');
        $twitter = \Social\Twitter::forge( $oauth_token,$oauth_token_secret);
        $content =   $twitter->get('account/verify_credentials');
        echo '<pre>';
        print_r($content);
        echo '</pre>';
        die;
    }
    
    public function action_getauth() {
        $gmtime = \Lib\Util::gmtime(date('Y/m/d H:i:s'));
        echo '<br/>api_auth_date = ' . $gmtime; 
        echo '<br/>api_auth_key = ' . hash('md5', Config::get('api_secret_key') . $gmtime);
        echo '<br/>-----------------------------------------------------------------';
        echo '<br/>GMT+0: ' . date('Y-m-d H:i:s', $gmtime);
    }
    
    public function action_curl() {
        $param = \Input::param();
        return Response::forge(View::forge('test/curl', $param));
    }
}