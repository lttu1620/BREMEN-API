<?php

/**
 * Any query in Model Place Sub Category
 *
 * @package Model
 * @created 2015-07-01
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Place_Sub_Category extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'type_id',
        'google_name',
        'name',
        'language_type',
        'category_type_id',
        'created',
        'updated',
        'disable'
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'place_sub_categories';

    /**
     * Add or update info for Place Sub Category
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Place Sub Category id or false if error
     */
    public static function add_update($param)
    {
        $id = !empty($param['id']) ? $param['id'] : 0;
        $place = new self;
        // check exist
        if (!empty($id)) {
            $place = self::find($id);
            if (empty($place)) {
                self::errorNotExist('place_sub_category_id');
                return false;
            }
        }
        // set value
        if (!empty($param['type_id'])) {
            $place->set('type_id', $param['type_id']);
        }
        if (isset($param['google_name'])) {
            $place->set('google_name', $param['google_name']);
        }
        if (isset($param['name'])) {
            $place->set('name', $param['name']);
        }
        if (isset($param['language_type'])) {
            $place->set('language_type', $param['language_type']);
        }
        if (isset($param['category_type_id'])) {
            $place->set('category_type_id', $param['category_type_id']);
        }
        // save to database
        if ($place->save()) {
            if (empty($place->id)) {
                $place->id = self::cached_object($place)->_original['id'];
            }
            return !empty($place->id) ? $place->id : 0;
        }
        return false;
    }

    /**
     * Get list Place Sub Category (using array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List Place Sub Category
     */
    public static function get_list($param)
    {
        $query = DB::select(
            self::$_table_name . '.*'
        )
            ->from(self::$_table_name);
        // filter by keyword
        if (!empty($param['id'])) {
            $query->where(self::$_table_name . '.id', '=', $param['id']);
        }
        if (!empty($param['type_id'])) {
            $query->where(self::$_table_name . '.type_id', '=', $param['type_id']);
        }
        if (!empty($param['language_type'])) {
            $query->where(self::$_table_name . '.language_type', '=', $param['language_type']);
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array('total' => $total, 'data' => $data);
    }

    /**
     * Get all Place Sub Category (without array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List Place Sub Category
     */
    public static function get_all($param)
    {
        $query = DB::select(
                self::$_table_name . '.*',
                array(Model_Place_Category::table() . '.id', 'category_id'),
                array(Model_Place_Category::table() . '.name', 'category_name')
            )
            ->from(self::$_table_name)
            ->join(Model_Place_Category::table())
            ->on(self::$_table_name . '.category_type_id', '=', Model_Place_Category::table() . '.type_id')
            ->where(self::$_table_name . '.disable', '=', '0')
            ->where(Model_Place_Category::table() . '.disable', '=', '0')
            ->where(self::$_table_name . '.language_type', '=', $param['language_type'])
            ->where(Model_Place_Category::table() . '.language_type', '=', $param['language_type']);
        // filter by keyword
        if (!empty($param['type_id'])) {
            $query->where(self::$_table_name . '.type_id', '=', $param['type_id']);
        }  
        if (!empty($param['category_type_id'])) {
            $query->where(self::$_table_name . '.category_type_id', '=', $param['category_type_id']);
        }
        $query->order_by(self::$_table_name . '.id', 'ASC');
        // get data
        $data = $query->execute()->as_array();
        return $data;
    }

    /**
     * Disable/Enable list Place Sub Category
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function disable($param)
    {
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $place = self::find($id);
            if ($place) {
                $place->set('disable', $param['disable']);
                if (!$place->save()) {
                    return false;
                }
            } else {
                self::errorNotExist('place_sub_category_id');
                return false;
            }
        }
        return true;
    }

    /**
     * Get detail Place Sub Category
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array|bool Detail Place Sub Category or false if error
     */
    public static function get_detail($param)
    {
        $data = self::find($param['id']);
        if (empty($data)) {
            static::errorNotExist('place_sub_category_id');
            return false;
        }
        return $data;
    }

    /**
     * Get detail Place Sub Category by Google name
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array|bool Detail Place Sub Category or false if error
     */
    public static function get_detail_by_google_name($param)
    {
        if (!is_array($param['google_name'])) {
            $param['google_name'] = explode(',', $param['google_name']);
        }
        if (empty($param['google_name'])) {
            return array();
        }
        $data = DB::select(
                self::$_table_name . '.*',
                array(Model_Place_Category::table() . '.id', 'category_id'),
                array(Model_Place_Category::table() . '.name', 'category_name')
            )
            ->from(self::$_table_name)
            ->join(Model_Place_Category::table())
            ->on(self::$_table_name . '.category_type_id', '=', Model_Place_Category::table() . '.type_id')
            ->where(self::$_table_name . '.disable', '=', '0')
            ->where(Model_Place_Category::table() . '.disable', '=', '0')
            ->where(self::$_table_name . '.language_type', '=', $param['language_type'])
            ->where(Model_Place_Category::table() . '.language_type', '=', $param['language_type'])
            ->where(self::$_table_name . '.google_name', 'IN', $param['google_name'])
            ->order_by(self::$_table_name . '.id', 'ASC')
            ->execute()
            ->as_array();
        return $data;  
    }
}
