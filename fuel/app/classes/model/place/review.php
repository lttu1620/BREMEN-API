<?php

/**
 * Any query in Model Place Review
 *
 * @package Model
 * @created 2015-06-29
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Place_Review extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'user_id',
        'place_id',      
        'comment',
        'language_type',
        'count_like',
        'count_comment',
        'created',
        'updated',
        'disable'
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'place_reviews';

    /**
     * Add or update info for Place Review
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Place Review id or false if error
     */
    public static function add_update($param)
    {        
        if (empty($param['review_point']) 
            && empty($param['comment']) 
            && empty($_FILES)) {
            self::errorParamInvalid('review_point_and_comment_and_images');
            return false;
        }
        $id = !empty($param['id']) ? $param['id'] : 0;
        // check exist
        if (!empty($id)) {
            $self = self::find($id);
            if (empty($self)) {
                self::errorNotExist('place_review_id');
                return false;
            }
            $param['place_id'] = $self->get('place_id');
        }
        $imagePath = array();
        if (!empty($_FILES)) {
            $uploadResult = \Lib\Util::uploadImage($thumb = 'places');            
            if ($uploadResult['status'] != 200) {
                self::setError($uploadResult['error']);            
                return false;
            }
            $imagePath = $uploadResult['body'];         
        }
        if (empty($param['place_id']) 
            && !empty($param['google_place_id'])) {
            if (empty($imagePath)) {
                $param['get_place_images'] = 1;
            }
            $param['place_id'] = Model_Place::add_place_by_google_place_id($param);
        }  
        if (empty($param['place_id'])) {
            static::errorNotExist('place_id');
            return false;
        }
        if (empty($self)) {
            $self = new self;            
            if (empty($param['count_like'])) {
                $param['count_like'] = '0';
            }
        }
        
        // set value     
        $self->set('user_id', $param['login_user_id']);
        $self->set('place_id', $param['place_id']);
        if (!isset($param['comment'])) {
            $param['comment'] = '';
        }
        $self->set('comment', $param['comment']);
        $self->set('language_type', $param['language_type']); 
        // save to database
        if ($self->save()) {
            if (empty($self->id)) {
                $self->id = self::cached_object($self)->_original['id'];
            }
            if (!empty($self->id)) {
                $param['favorite_type'] = 2;
                $options['where'] = array(
                    'place_id' => $param['place_id'],
                    'user_id' => $param['login_user_id'],
                );
                $favorite = Model_Place_Favorite::find('first', $options);
                if (empty($favorite)) {
                    $favorite = new Model_Place_Favorite;
                    $favorite->set('place_id', $param['place_id']);
                    $favorite->set('user_id', $param['login_user_id']);
                    $favorite->set('favorite_type', $param['favorite_type']);
                    if (!$favorite->create()) {
                        return false;
                    }
                } elseif (($favorite->get('favorite_type')&2) == 0) {                   
                    $favorite->set('favorite_type', ($favorite->get('favorite_type') | $param['favorite_type']));
                    $favorite->set('disable', '0');
                    if (!$favorite->update()) {
                        return false;
                    }
                }
                if (isset($param['review_point']) && $param['review_point'] > 0) {
                    Model_Place_Review_Point_Log::add(
                        array(
                            'login_user_id' => $param['login_user_id'],
                            'place_id' => $param['place_id'],
                            'place_review_id' => $self->id,
                            'review_point' => $param['review_point'],
                        )
                    );
                    if (self::error()) {
                        return false;
                    }
                }
                if (!empty($imagePath)) {
                    Model_Place_Image::upload_images_for_review(
                        array(
                            'user_id' => $param['login_user_id'],
                            'place_id' => $param['place_id'],
                            'place_review_id' => $self->id,
                            'image_path' => $imagePath,
                        )
                    );
                    if (self::error()) {
                        return false;
                    }
                }
            }
            return !empty($self->id) ? $self->id : 0;
        }
        return false;
    }

    /**
     * Get list Place Review (using array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List Place Review
     */
    public static function get_list($param)
    {
        if (empty($param['user_id'])) {
            $param['user_id'] = 0;
        }
        if (empty($param['login_user_id'])) {
            $param['login_user_id'] = 0;
        }
        $query = DB::select(
                self::$_table_name.'.id',
                self::$_table_name.'.user_id',
                self::$_table_name.'.place_id',
                self::$_table_name.'.comment',
                self::$_table_name.'.created',
                'users.name',
                'users.image_path',
                'users.sex_id',
                'users.email',
                'users.birthday',
                'users.zipcode',
                DB::expr("IFNULL(place_review_point_logs.review_point, 0) AS review_point"),
                DB::expr("IF(ISNULL(place_review_likes.id),0,1) AS is_like"),
                DB::expr("IF(ISNULL(follow_users.id),0,1) AS is_follow_user")
            )
            ->from(self::$_table_name)
            ->join('users')
            ->on(self::$_table_name.'.user_id', '=', 'users.id')
            ->join('place_review_point_logs', 'LEFT')
            ->on(self::$_table_name.'.place_id', '=', 'place_review_point_logs.place_id')
            ->on(self::$_table_name.'.id', '=', 'place_review_point_logs.place_review_id')
            ->on(self::$_table_name.'.user_id', '=', 'place_review_point_logs.user_id')
            ->join(
                DB::expr(
                    "
                        (SELECT *
                        FROM place_review_likes
                        WHERE user_id = {$param['login_user_id']}
                            AND disable = 0) place_review_likes
                    "
                ),
                'LEFT'
            )
            ->on(self::$_table_name.'.id', '=', 'place_review_likes.place_review_id')
            ->join(
                DB::expr(
                    "
                (SELECT *
                FROM follow_users
                WHERE user_id = {$param['login_user_id']}
                    AND disable = 0) follow_users
            "
                ),
                'LEFT'
            )
            ->on(self::$_table_name.'.user_id', '=', 'follow_users.follow_user_id')
            ->where(self::$_table_name.'.disable', '=', '0');
        // filter by keyword
        if (!empty($param['user_id'])) {
            $query->where(self::$_table_name.'.user_id', '=', $param['user_id']);
        }
        if (!empty($param['place_id'])) {
            $query->where(self::$_table_name.'.place_id', '=', $param['place_id']);
        }
        if (!empty($param['name'])) {
            $query->where(self::$_table_name.'.name', 'LIKE', "%{$param['name']}%");
        }
        if (!empty($param['name_kana'])) {
            $query->where(self::$_table_name.'.name_kana', 'LIKE', "%{$param['name_kana']}%");
        }
        if (!empty($param['comment'])) {
            $query->where(self::$_table_name.'.comment', 'LIKE', "%{$param['comment']}%");
        }
        if (isset($param['disable']) && $param['disable'] !== '') {
            $query->where(self::$_table_name.'.disable', '=', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name.'.'.$sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name.'.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        if (isset($param['get_place_images']) && !empty($data)) {
            $placeReviewId = Lib\Arr::field($data, 'id');
            $images = Model_Place_Image::get_all(
                array(
                    'place_review_id' => $placeReviewId,
                )
            );
            foreach ($data as &$row) {
                $row['place_images'] = Lib\Arr::filter($images, 'place_review_id', $row['id'], false, false);
            }
            unset($row);
        }
        return array('total' => $total, 'data' => $data);
    }

    /**
     * Get all Place Review (without array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List Place Review
     */
    public static function get_all($param)
    {
        if (empty($param['user_id'])) {
            $param['user_id'] = 0;
        }
        if (empty($param['login_user_id'])) {
            $param['login_user_id'] = 0;
        }
        $query = DB::select(
                self::$_table_name.'.id',
                self::$_table_name.'.user_id',
                self::$_table_name.'.place_id',
                self::$_table_name.'.comment',
                self::$_table_name.'.created',
                'users.name',
                DB::expr("IFNULL(IF(users.image_path='',NULL,users.image_path),'" . \Config::get('no_image_user') . "') AS image_path"),
                'users.sex_id',
                'users.email',
                'users.birthday',
                'users.zipcode',
                DB::expr("IFNULL(place_review_point_logs.review_point, 0) AS review_point"),
                DB::expr("IF(ISNULL(place_review_likes.id),0,1) AS is_like"),
                DB::expr("IF(ISNULL(follow_users.id),0,1) AS is_follow_user")
            )
            ->from(self::$_table_name)
            ->join('users')
            ->on(self::$_table_name.'.user_id', '=', 'users.id')
            ->join('place_review_point_logs', 'LEFT')
            ->on(self::$_table_name.'.place_id', '=', 'place_review_point_logs.place_id')
            ->on(self::$_table_name.'.id', '=', 'place_review_point_logs.place_review_id')
            ->on(self::$_table_name.'.user_id', '=', 'place_review_point_logs.user_id')
            ->join(
                DB::expr(
                    "
                        (SELECT *
                        FROM place_review_likes
                        WHERE user_id = {$param['login_user_id']}
                            AND disable = 0) place_review_likes
                    "
                ),
                'LEFT'
            )
            ->on(self::$_table_name.'.id', '=', 'place_review_likes.place_review_id')
            ->join(
                DB::expr(
                    "
                        (SELECT *
                        FROM follow_users
                        WHERE user_id = {$param['login_user_id']}
                            AND disable = 0) follow_users
                    "
                ),
                'LEFT'
            )
            ->on(self::$_table_name.'.user_id', '=', 'follow_users.follow_user_id')
            ->where(self::$_table_name.'.disable', '=', '0');
        // filter by keyword
        if (!empty($param['user_id'])) {
            $query->where(self::$_table_name.'.user_id', '=', $param['user_id']);
        }
        if (!empty($param['place_id'])) {
            if (!is_array($param['place_id'])) {
                $param['place_id'] = array($param['place_id']);
            }
            $query->where(self::$_table_name.'.place_id', 'IN', $param['place_id']);
        }
        if (!empty($param['language_type'])) {
            //$query->where(self::$_table_name . '.language_type', '=', $param['language_type']);
        }
        if (!empty($param['limit'])) {
            $query->limit($param['limit'])->offset(0);
        }
        $query->order_by(self::$_table_name.'.created', 'DESC');
        $data = $query->execute()->as_array();

        if (isset($param['get_place_images']) && !empty($data)) {
            $placeReviewId = Lib\Arr::field($data, 'id');
            $images = Model_Place_Image::get_all(
                array(
                    'place_review_id' => $placeReviewId,
                )
            );
            foreach ($data as &$row) {
                $row['place_images'] = Lib\Arr::filter($images, 'place_review_id', $row['id'], false, false);
            }
            unset($row);
        }
        return $data;
    }

    /**
     * Disable/Enable list Place Review
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function disable($param)
    {
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $place = self::find($id);
            if ($place) {
                $place->set('disable', $param['disable']);
                if (!$place->save()) {
                    return false;
                }
            } else {
                self::errorNotExist('place_review_id');
                return false;
            }
        }
        return true;
    }

    /**
     * Get detail Place Review
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array|bool Detail Place Review or false if error
     */
    public static function get_detail($param)
    {
        if (empty($param['login_user_id'])) {
            $param['login_user_id'] = 0;
        }
        $data = DB::select(
                self::$_table_name.'.id',
                self::$_table_name.'.user_id',
                self::$_table_name.'.comment',
                self::$_table_name.'.created',
                self::$_table_name.'.place_id',
                'users.name',
                'users.image_path',
                'users.sex_id',
                'users.email',
                'users.birthday',
                'users.zipcode',
                DB::expr("IFNULL(place_review_point_logs.review_point, 0) AS review_point"),
                DB::expr("IF(ISNULL(place_review_likes.id),0,1) AS is_like"),
                DB::expr("IF(ISNULL(follow_users.id),0,1) AS is_follow_user")
            )
            ->from(self::$_table_name)
            ->join('users')
            ->on(self::$_table_name.'.user_id', '=', 'users.id')
            ->join('place_review_point_logs', 'LEFT')
            ->on(self::$_table_name.'.place_id', '=', 'place_review_point_logs.place_id')
            ->on(self::$_table_name.'.id', '=', 'place_review_point_logs.place_review_id')
            ->on(self::$_table_name.'.user_id', '=', 'place_review_point_logs.user_id')
            ->join(
                DB::expr(
                    "
                        (SELECT *
                        FROM place_review_likes
                        WHERE user_id = {$param['login_user_id']}
                            AND disable = 0) place_review_likes
                    "
                ),
                'LEFT'
            )
            ->on(self::$_table_name.'.id', '=', 'place_review_likes.place_review_id')
            ->join(
                DB::expr(
                    "
                (SELECT *
                FROM follow_users
                WHERE user_id = {$param['login_user_id']}
                    AND disable = 0) follow_users
            "
                ),
                'LEFT'
            )
            ->on(self::$_table_name.'.user_id', '=', 'follow_users.follow_user_id')
            ->where(self::$_table_name.'.disable', '=', '0')
            ->where(self::$_table_name.'.id', '=', $param['id'])
            ->execute()
            ->offsetGet(0);
        if (empty($data)) {
            static::errorNotExist('place_review_id');
            return false;
        }
        $data = Model_Place_Information::merge_info(
            $data,
            $param['language_type'],
             '',
            'place_id',           
            array(
                'place_informations.name' => 'place_name',
                'place_informations.name_kana' => 'place_name_kana',
                'place_informations.address' => 'place_address',
            )
        );
        if (isset($param['get_like'])) {
            $place_review_likes = Model_Place_Review_Like::get_all_by_place_review_id(
                array(
                    'place_review_id' => $param['id'],
                    'login_user_id'   => $param['login_user_id'],
                )
            );
            if (!empty($place_review_likes)) {
                $userA = '';
                foreach ($place_review_likes as $like) {
                    if ($like['user_id'] == $param['login_user_id']) {
                        $userA = $like['name'];
                    }
                }
                if (empty($userA)) {
                    $userA = $place_review_likes[0]['name'];
                }
                $data['notice'] = Model_Notice::get_notice_string(
                    array(
                        'type' => 2,
                        'count_like' => count($place_review_likes) - 1,
                        'name' => $data['name'],
                        'place_name' => $data['place_name'],
                        'language_type' => $param['language_type'],
                    )
                );
                $data['place_review_likes'] = $place_review_likes;
            }
        }
        if (isset($param['get_comment'])) {
            $data['place_review_comments'] = Model_Place_Review_Comment::get_all_comment_by_place_review_id(
                array(
                    'place_review_id' => $param['id'],
                    'login_user_id'   => $param['login_user_id'],
                )
            );
        }
        return $data;
    }
    
    /**
     * Get all review for timeline of User
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List place
     */
    public static function get_last_review($param)
    {
        if (empty($param['user_id'])) {
            $param['user_id'] = 0;
        }
        if (empty($param['login_user_id'])) {
            $param['login_user_id'] = 0;
        }
        $query = DB::select(
            self::$_table_name.'.id',
            self::$_table_name.'.place_id',
            self::$_table_name.'.comment',
            self::$_table_name.'.created',
            DB::expr("IFNULL(place_review_point_logs.review_point, 0) AS review_point"),
            DB::expr("IF(ISNULL(place_review_likes.id),0,1) AS is_like"),
            self::$_table_name.'.user_id',
            'users.name',
            'users.image_path',
            'users.sex_id',
            'users.email',
            'users.birthday',
            'users.zipcode',
            DB::expr("IF(ISNULL(follow_users.id),0,1) AS is_follow_user")
        )
            ->from(self::$_table_name)
            ->join(
                DB::expr(
                    "
                (   SELECT place_id, max(id) AS id
                    FROM place_reviews
                    WHERE disable = 0
                    GROUP BY place_id
                ) last_place_reviews
            "
                ),
                'LEFT'
            )
            ->on(self::$_table_name.'.place_id', '=', 'last_place_reviews.place_id')
            ->on(self::$_table_name.'.id', '=', 'last_place_reviews.id')
            ->join('users')
            ->on(self::$_table_name.'.user_id', '=', 'users.id')
            ->join('place_review_point_logs', 'LEFT')
            ->on(self::$_table_name.'.place_id', '=', 'place_review_point_logs.place_id')
            ->on(self::$_table_name.'.id', '=', 'place_review_point_logs.place_review_id')
            ->on(self::$_table_name.'.user_id', '=', 'place_review_point_logs.user_id')
            ->join(
                DB::expr(
                    "
                (SELECT *
                FROM place_review_likes
                WHERE user_id = {$param['login_user_id']}
                    AND disable = 0) place_review_likes
            "
                ),
                'LEFT'
            )
            ->on(self::$_table_name.'.id', '=', 'place_review_likes.place_review_id')
            ->join(
                DB::expr(
                    "
                (SELECT *
                FROM follow_users
                WHERE user_id = {$param['login_user_id']}
                    AND disable = 0) follow_users
            "
                ),
                'LEFT'
            )
            ->on(self::$_table_name.'.user_id', '=', 'follow_users.follow_user_id')
            ->where(self::$_table_name.'.disable', '=', '0');
        // filter by keyword
        if (!empty($param['user_id'])) {
            $query->where(self::$_table_name.'.user_id', '=', $param['user_id']);
        }
        if (!empty($param['place_id'])) {
            if (!is_array($param['place_id'])) {
                $param['place_id'] = array($param['place_id']);
            }
            $query->where(self::$_table_name.'.place_id', 'IN', $param['place_id']);
        }
        if (!empty($param['language_type'])) {
            //$query->where(self::$_table_name . '.language_type', '=', $param['language_type']);
        }
        if (!empty($param['limit'])) {
            $query->limit($param['limit'])->offset(0);
        }
        $query->order_by(self::$_table_name.'.id', 'ASC');
        $data = $query->execute()->as_array();
        return $data;
    }

}
