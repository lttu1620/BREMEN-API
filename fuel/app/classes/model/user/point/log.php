<?php

/**
 * Any query in Model User Point Log
 *
 * @package Model
 * @created 2015-06-17
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_User_Point_Log extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'user_id',
        'point',
        'created'
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'user_point_logs';

    /**
     * Add info for User Point Log
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool User Point Log id or false if error
     */
    public static function add($param)
    {
        $log = new self;
        $log->set('user_id', $param['user_id']);
        $log->set('point', $param['point']);
        if ($log->create()) {
            if (!Model_User::add_update(array(
                'id'    => $param['user_id'],
                'point' => $param['point']
            ))) {
                return false;
            }
            $log->id = self::cached_object($log)->_original['id'];
            return !empty($log->id) ? $log->id : 0;
        }
        return false;
    }

    /**
     * Get list User Point Log (using array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List User Point Log
     */
    public static function get_list($param)
    {
        $query = DB::select(
            self::$_table_name . '.*',
            'users.name',
            'users.email'
        )
            ->from(self::$_table_name)
            ->join('users')
            ->on(self::$_table_name . '.user_id', '=', 'users.id');
        // filter by keyword
        if (!empty($param['user_id'])) {
            $query->where('user_id', '=', $param['user_id']);
        }
        if (!empty($param['username'])) {
            $query->where('users.name', 'LIKE', "%{$param['username']}%");
        }
        if (!empty($param['email'])) {
            $query->where('users.email', 'LIKE', "%{$param['email']}%");
        }
        if (!empty($param['date_from'])) {
            $query->where(self::$_table_name . '.created', '>=', self::date_from_val($param['date_from']));
        }
        if (!empty($param['date_to'])) {
            $query->where(self::$_table_name . '.created', '<=', self::date_to_val($param['date_to']));
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array('total' => $total, 'data' => $data);
    }
}
