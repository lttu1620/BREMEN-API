<?php

/**
 * Any query in Model Notice
 *
 * @package Model
 * @created 2015-07-08
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Notice extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'receive_user_id',
        'user_id',
        'place_id',
        'place_review_id',
        'place_review_comment_id',
        'type',
        'is_read',       
        'created',
        'updated',
        'disable'
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'notices';

    /**
     * Get list Notice (using array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List Notice
     */
    public static function get_list($param)
    {
        $param['page'] = !empty($param['page']) ? $param['page'] : Config::get('page_default', 1);
        $param['limit'] = !empty($param['limit']) ? $param['limit'] : Config::get('limit_default', 10);
        $offset = ($param['page'] - 1) * $param['limit']; 
        
        $query = DB::select(
                self::$_table_name.'.*',                                             
                array('place_informations.name', 'place_name'),
                'users.name',  
                DB::expr("(SELECT count_follow FROM users WHERE disable = 0 AND id={$param['login_user_id']}) AS count_follow"),
                'place_reviews.count_like',
                'place_reviews.count_comment',
                'place_review_comments.comment'
            )
            ->from(self::$_table_name)          
            ->join('users')
            ->on(self::$_table_name.'.user_id', '=', 'users.id')
                        
            ->join('places', 'LEFT')
            ->on(self::$_table_name.'.place_id', '=', 'places.id')
                        
            ->join('place_informations', 'LEFT')
            ->on(self::$_table_name.'.place_id', '=', 'place_informations.place_id')
                        
            ->join('place_reviews', 'LEFT')
            ->on(self::$_table_name.'.place_review_id', '=', 'place_reviews.id')
            ->and_on(self::$_table_name.'.place_id', '=', 'place_reviews.place_id')
                        
            ->join('place_review_likes', 'LEFT')
            ->on(self::$_table_name.'.user_id', '=', 'place_review_likes.user_id')
            ->and_on(self::$_table_name.'.place_review_id', '=', 'place_review_likes.place_review_id')
                                    
            ->join('place_review_comments', 'LEFT')
            ->on(self::$_table_name.'.user_id', '=', 'place_review_comments.user_id')
            ->and_on(self::$_table_name.'.place_review_id', '=', 'place_review_comments.place_review_id')            
            ->and_on(self::$_table_name.'.place_review_comment_id', '=', 'place_review_comments.id')
                                    
            ->where(self::$_table_name.'.disable', '0')
            ->where(self::$_table_name.'.receive_user_id', $param['login_user_id'])            
            ->where(DB::expr("(notices.place_id = 0 OR place_informations.language_type='{$param['language_type']}')"))            
            ->where(DB::expr("(notices.place_id = 0 OR places.disable='0')"))            
            ->where(DB::expr("(notices.place_review_id = 0 OR place_reviews.disable='0')"))            
            ->where(DB::expr("(notices.place_review_id = 0 OR place_review_likes.disable='0')"))      
            ->where(DB::expr("(notices.place_review_comment_id = 0 OR place_review_comments.disable='0')"));
        if (!empty($param['user_id'])) {
            $query->where('notices.user_id', $param['user_id']);
        }
        if (!empty($param['type'])) {
            $query->where('notices.type', $param['type']);           
        }
        if (!empty($param['place_id'])) {
            $query->where('notices.place_id', $param['place_id']);   
        }
        if (!empty($param['place_review_id'])) {
            $query->where('notices.place_review_id', $param['place_review_id']);
        }
        if (!empty($param['place_review_comment_id'])) {
            $query->where('notices.place_review_comment_id', $param['place_review_comment_id']);
        }
        $query->order_by('notices.created', 'DESC')
            ->limit($param['limit'])->offset($offset);
        $data = $query->execute()->as_array();           
        $total = !empty($data) ? DB::count_last_query() : 0;
        if ($data) {            
            // find place_review_id of login user
            $place_review_id = array();
            foreach ($data as $item) {
                if (!empty($item['place_review_id']) && !in_array($item['place_review_id'], $place_review_id)) {
                    $place_review_id[] = $item['place_review_id'];
                }
            }
            $place_review_id = !empty($place_review_id) ? implode(',', $place_review_id) : 0;
            $no_image_user = \Config::get('no_image_user');  
            $users = DB::select(
                array('users.id', 'user_id'),                                 
                array('users.name', 'name'),                                 
                DB::expr("IFNULL(IF(image_path='',NULL,image_path),'{$no_image_user}') AS image_path"),
                'place_review_id', 
                'type'
            )              
            ->from('users')            
            ->join(DB::expr("(
                        SELECT CONCAT('follow_user', id) AS id, user_id, 0 AS place_review_id, '1' AS 'type'
                        FROM follow_users
                        WHERE disable = 0 
                        AND follow_user_id = {$param['login_user_id']}

                        UNION

                        SELECT CONCAT('place_review_like', id) AS id, user_id, place_review_id, '2' AS 'type'
                        FROM place_review_likes
                        WHERE disable = 0
                        AND place_review_id IN ({$place_review_id})

                        UNION

                        SELECT CONCAT('place_review_comment', id) AS id, user_id, place_review_id, '3' AS 'type'
                        FROM place_review_comments
                        WHERE disable = 0
                        AND place_review_id IN ({$place_review_id})               
                    ) A")
                )          
            ->on('users.id', '=', 'A.user_id')
            ->where('users.disable', '0')
            ->execute()
            ->as_array();            
            foreach ($data as &$item) {
                $item['users'] = array();
                foreach ($users as $user) {
                    if ($item['type'] == $user['type'] 
                        && $item['place_review_id'] == $user['place_review_id']                       
                    ) {
                        $item['users'][$user['user_id']] = $user;
                    }
                } 
                $item['users'] = array_values($item['users']);
                if ($item['type'] == 2) {
                    $item['count_like'] = count($item['users']);                
                }
                if ($item['type'] == 3) {
                    $item['count_comment'] = count($item['users']);
                }                
                $item['notice'] = self::get_notice_string(
                    array(
                        'type' => $item['type'],
                        'language_type' => $param['language_type'],
                        'name' => $item['name'],
                        'place_name' => !empty($item['place_name']) ? $item['place_name'] : '',
                        'count_follow' => !empty($item['count_follow']) ? $item['count_follow'] - 1 : 0,
                        'count_like' => !empty($item['count_like']) ? $item['count_like'] - 1 : 0,
                        'count_comment' => !empty($item['count_comment']) ? $item['count_comment'] - 1 : 0,
                    )
                );
            }
            unset($item);
        }
        return array('total' => $total, 'data' => $data);        
    }

    /**
     * Get notice string
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List Notice
     */
    public static function get_notice_string($param)
    {
        $notice = '';
        switch ($param['type']) {
            case 1: // follow user
                $typeString = $param['count_follow'] == 0 ? 'single' : 'multi';
                $notice = Config::get('notice_string')[$param['type']][$param['language_type']][$typeString];
                switch ($param['language_type']) {
                    case 1: // jp - japanese
                        switch ($typeString) {
                            case 'single':
                                $notice = sprintf($notice, $param['name']);
                                break;
                            case 'multi':
                                $notice = sprintf($notice, $param['name'], $param['count_follow']);
                                break;
                        }
                        break;
                    case 2: // en - english
                        switch ($typeString) {
                            case 'single':
                                $notice = sprintf($notice, $param['name']);
                                break;
                            case 'multi':
                                $other = $param['count_follow'] > 1 ? ' others' : ' other';
                                $notice = sprintf($notice, $param['name'], $param['count_follow'].$other);
                                break;
                        }
                        break;
                    case 3: // th - thai
                        switch ($typeString) {
                            case 'single':
                                $notice = sprintf($notice, $param['name']);
                                break;
                            case 'multi':
                                $other = $param['count_follow'] > 1 ? ' others' : ' other';
                                $notice = sprintf($notice, $param['name'], $param['count_follow'].$other);
                                break;
                        }
                        break;
                    case 4: // vn - vietnamese
                        switch ($typeString) {
                            case 'single':
                                $notice = sprintf($notice, $param['name']);
                                break;
                            case 'multi':
                                $other = $param['count_follow'] > 1 ? ' others' : ' other';
                                $notice = sprintf($notice, $param['name'], $param['count_follow'].$other);
                                break;
                        }
                        break;
                }
                break;
            case 2: // like review
                $typeString = $param['count_like'] == 0 ? 'single' : 'multi';
                $notice = Config::get('notice_string')[$param['type']][$param['language_type']][$typeString];
                switch ($param['language_type']) {
                    case 1: // jp - japanese
                        switch ($typeString) {
                            case 'single':
                                $notice = sprintf($notice, $param['name'], $param['place_name']);
                                break;
                            case 'multi':
                                $notice = sprintf($notice, $param['name'], $param['count_like'], $param['place_name']);
                                break;
                        }
                        break;
                    case 2: // en - english
                        switch ($typeString) {
                            case 'single':
                                $notice = sprintf($notice, $param['name'], $param['place_name']);
                                break;
                            case 'multi':
                                $other = $param['count_like'] > 1 ? ' others' : ' other';
                                $notice = sprintf($notice, $param['name'], $param['count_like'].$other, $param['place_name']);
                                break;
                        }
                        break;
                    case 3: // th - thai
                        switch ($typeString) {
                            case 'single':
                                $notice = sprintf($notice, $param['name'], $param['place_name']);
                                break;
                            case 'multi':
                                $other = $param['count_like'] > 1 ? ' others' : ' other';
                                $notice = sprintf($notice, $param['name'], $param['count_like'].$other, $param['place_name']);
                                break;
                        }
                        break;
                    case 4: // vn - vietnamese
                        switch ($typeString) {
                            case 'single':
                                $notice = sprintf($notice, $param['name'], $param['place_name']);
                                break;
                            case 'multi':
                                $other = $param['count_like'] > 1 ? ' others' : ' other';
                                $notice = sprintf($notice, $param['name'], $param['count_like'].$other, $param['place_name']);
                                break;
                        }
                        break;
                }
                break;
            case 3: // comment review
                $typeString = $param['count_comment'] == 0 ? 'single' : 'multi';
                $notice = Config::get('notice_string')[$param['type']][$param['language_type']][$typeString];
                switch ($param['language_type']) {
                    case 1: // jp - japanese
                        switch ($typeString) {
                            case 'single':
                                $notice = sprintf($notice, $param['name'], $param['place_name']);
                                break;
                            case 'multi':
                                $notice = sprintf($notice, $param['name'], $param['count_comment'], $param['place_name']);
                                break;
                        }
                        break;
                    case 2: // en - english
                        switch ($typeString) {
                            case 'single':
                                $notice = sprintf($notice, $param['name'], $param['place_name']);
                                break;
                            case 'multi':
                                $other = $param['count_comment'] > 1 ? ' others' : ' other';
                                $notice = sprintf($notice, $param['name'], $param['count_comment'].$other, $param['place_name']);
                                break;
                        }
                        break;
                    case 3: // th - thai
                        switch ($typeString) {
                            case 'single':
                                $notice = sprintf($notice, $param['name'], $param['place_name']);
                                break;
                            case 'multi':
                                $other = $param['count_comment'] > 1 ? ' others' : ' other';
                                $notice = sprintf($notice, $param['name'], $param['count_comment'].$other, $param['place_name']);
                                break;
                        }
                        break;
                    case 4: // vn - vietnamese
                        switch ($typeString) {
                            case 'single':
                                $notice = sprintf($notice, $param['name'], $param['place_name']);
                                break;
                            case 'multi':
                                $other = $param['count_comment'] > 1 ? ' others' : ' other';
                                $notice = sprintf($notice, $param['name'], $param['count_comment'].$other, $param['place_name']);
                                break;
                        }
                        break;
                }
                break;
        }
        return $notice;
    }

    /**
     * Check read for Notice
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function is_read($param)
    {
        if (empty($param['id'])) {
            return false;
        }
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $notice = self::find($id);
            if (!empty($notice)) {
                $notice->set('is_read', $param['is_read']);
                if (!$notice->save()) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Get detail Notice
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array|bool Detail Notice or false if error
     */
    public static function get_detail($param)
    {
        $data = self::find($param['id']);
        if (empty($data)) {
            static::errorNotExist('notice_id');
            return false;
        }
        $type = $data->get('type');
        $receiveUserId = $data->get('receive_user_id');
        $placeReviewId = $data->get('place_review_id');
        $sql = '';
        switch ($type) {
            case 1:
                $sql = "
                    SELECT follow_users.*,
                           users.name
                    FROM follow_users
                    JOIN users ON follow_users.user_id = users.id
                    WHERE follow_user_id = {$receiveUserId}
                        AND follow_users.disable = 0
                        AND users.disable = 0
                    ORDER BY follow_users.created DESC
                ";
                break;
            case 2:
                $sql = "
                    SELECT places.name AS place_name,
                           place_review_id,
                           place_review_likes.user_id,
                           users.name
                    FROM place_review_likes
                    JOIN users ON place_review_likes.user_id = users.id
                    JOIN place_reviews ON place_review_likes.place_review_id = place_reviews.id
                    LEFT JOIN
                        ( SELECT places.id,
                                 place_informations.name
                         FROM place_informations
                         JOIN places ON place_informations.place_id = places.id
                         WHERE places.disable = 0
                             AND place_informations.disable = 0) places ON place_reviews.place_id = places.id
                    WHERE place_reviews.user_id = {$receiveUserId}
                        AND place_reviews.id = {$placeReviewId}
                        AND place_review_likes.disable = 0
                        AND users.disable = 0
                        AND place_reviews.disable = 0
                ";
                break;
            case 3:
                $sql = "
                    SELECT places.name AS place_name,
                           place_review_id,
                           place_review_comments.user_id,
                           place_review_comments.comment,
                           users.name
                    FROM place_review_comments
                    JOIN users ON place_review_comments.user_id = users.id
                    JOIN place_reviews ON place_review_comments.place_review_id = place_reviews.id
                    LEFT JOIN
                        (SELECT places.id,
                                place_informations.name
                         FROM place_informations
                         JOIN places ON place_informations.place_id = places.id
                         WHERE places.disable = 0
                             AND place_informations.disable = 0) places ON place_reviews.place_id = places.id
                    WHERE place_reviews.user_id = {$receiveUserId}
                        AND place_reviews.id = {$placeReviewId}
                        AND place_review_comments.disable = 0
                        AND users.disable = 0
                        AND place_reviews.disable = 0
                ";
                break;
        }
        $notice = DB::query($sql)->execute()->as_array();
        $data['notice'] = !empty($notice) ? $notice : array();
        return $data;
    }
    
    /**
     * Add message to Push message
     *
     * @author thailh
     * @param array $param Input data
     * @return bool true or false if error
     */
    public static function add_push_message($obj, $param)
    { 
        if (empty($obj)) {
            self::errorParamInvalid();
            return false;
        }
        if ($obj instanceof Model_Place_Review_Comment) {
            $review = Model_Place_Review::find($obj->get('place_review_id'));            
            $param['type'] = \Config::get('notices.type.comment_review');            
            $param['setting_name'] = 'comment_review';            
        } elseif ($obj instanceof Model_Place_Review_Like) {
            $review = Model_Place_Review::find($obj->get('place_review_id'));
            $param['type'] = \Config::get('notices.type.like_review'); 
            $param['setting_name'] = 'like_review';
        }
        if (empty($review)) {
            self::errorNotExist('place_review_id', $obj->get('place_review_id'));
            return false;
        }
        $notice = self::get_list(array(
            'login_user_id' => $review->get('user_id'),
            'user_id' => $obj->get('user_id'),                
            'place_review_id' => $review->get('id'),
            'place_review_comment_id' => $obj->get('id'),
            'type' => $param['type'],
            'language_type' => $param['language_type'],
            'limit' => 1,
        ));
        if (empty($notice['data'][0]['notice'])) {
            // notice not exists, ignore send message
            return true;
        } else {
            $param['message'] = json_encode($notice['data'][0]);
        }           
        $self = self::find(
            'first',
            array(
                'where' => array(
                    'user_id' => $review->get('user_id'),
                )
            )
        );
        if (empty($self)) {
            // user not regist yet device_id, ignore send message
            return true;
        } 
        if (!Model_Push_Message::add_update(array(
            'receive_user_id' => $review->get('user_id'),
            'message' => $param['message'],
            'is_sent' => 0,            
        ))) {
            \LogLib::error('Can not insert to push_message', __METHOD__, $param);
            return false; 
        }        
        return true;
    }
    
}
