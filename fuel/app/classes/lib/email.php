<?php
/**
 * Support functions for Email
 *
 * @package Lib
 * @created 2014-11-25
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
namespace Lib;

use Fuel\Core\Config;

class Email {

    public static function beforeSend() {
        $send = Config::get('send_email', true);
        if ($send == false) {
            return false;
        }
        return true;
    }

    /**
     * Send email to test email (For testing)
     *
     * @author thailh
     * @param string email Email
     * @return string Real email | test email
     */
    public static function to($email) {
        $test_email = Config::get('test_email', '');
        return !empty($test_email) ? $test_email : $email;
    }
    
    /**
     * Send email if forgetting password
     *
     * @author thailh
     * @param array $param Information for sending email
     * @return bool Return true if successful ortherwise return false
     */
    public static function sendForgetPasswordEmail($param) {
        if (self::beforeSend() == false) {
            return true;
        }
        $to = $param['email'];
        $subject = 'パスワード再設定手続きのお知らせ';
       
        $email = \Email::forge('jis');
        $email->from(Config::get('system_email.noreply'), 'Bremen No reply');
        $email->subject($subject);

        $data['token']= $param['token'];
        $data['user_name']= $param['name'];
        $body = \View::forge('email/pc/forget_password', $data);   
        $email->html_body($body);
        $email->to(self::to($to));
        $ok = 0;
        try {
            \LogLib::info("Sent email to {$to}", __METHOD__, $param);
            if ($email->send()) {
                $ok = 1;
            }
        } catch (\EmailSendingFailedException $e) {
            \LogLib::warning($e, __METHOD__, $param);            
        } catch (\EmailValidationFailedException $e) {
    		\LogLib::warning($e, __METHOD__, $param);
    	}
        \Model_Mail_Send_Log::add(array(
            'user_id' => !empty($param['user_id']) ? $param['user_id'] : 0,           
            'type' => \Model_Mail_Send_Log::TYPE_EMAIL_FORGET_PASSWORD,
            'title' => $subject,
            'content' => $body,
            'to_email' => $to,
            'status' => $ok
        ));
        return (boolean) $ok;
    }

    /**
     * Send email if forgetting password for mobile only   
     *  
     * @author thailh
     * @param array $param Information for sending email
     * @return bool Return true if successful ortherwise return false
     */
    public static function sendForgetPasswordEmailForMobile($param) {
        if (self::beforeSend() == false) {
            return true;
        }
        $to = $param['email'];
        $subject = 'パスワード再設定手続きのお知らせ';
        
        $email = \Email::forge('jis');
        $email->from(Config::get('system_email.noreply'), 'Bremen No reply');
        $email->subject($subject);
        
        $data['user_name'] = $param['name'];
        $data['token'] = $param['token'];
        $body = \View::forge('email/mobile/forget_password', $data);   
        $email->html_body($body);
        $email->to(self::to($to));
        $ok = 0;
        try {
            \LogLib::info("Sent email to {$to}", __METHOD__, $param);
            if ($email->send()) {
                $ok = 1;
            }
        } catch (\EmailSendingFailedException $e) {
            \LogLib::warning($e, __METHOD__, $param);            
        } catch (\EmailValidationFailedException $e) {
    		\LogLib::warning($e, __METHOD__, $param);
    	}
        \Model_Mail_Send_Log::add(array(
            'user_id' => !empty($param['user_id']) ? $param['user_id'] : 0,           
            'type' => \Model_Mail_Send_Log::TYPE_EMAIL_FORGET_PASSWORD,
            'title' => $subject,
            'content' => $body,
            'to_email' => $to,
            'status' => $ok
        ));
        return (boolean) $ok;
    }
    
     /**
     * Send create user
     *
     * @author thailh
     * @param array $param Information for sending email
     * @return bool Return true if successful ortherwise return false
     */
    public static function sendCreateUser($param) {
        if (self::beforeSend() == false) {
            return true;
        }
        return true;
        $subject = 'Bremen 仮登録';
        $to = $param['email'];
        $email = \Email::forge('jis');
        $email->from(Config::get('system_email.noreply'), 'Bremen No reply');
        $email->subject($subject);
        $data['url']= Config::get('fe_url') . "login";
        $data['password'] = $param['password'];
        $body = \View::forge('email/pc/create_user', $data);
        $email->html_body($body);
        $email->to(self::to($to));
        $ok = 0;
        try {
            \LogLib::info("Sent email to {$to}", __METHOD__, $param);
            if ($email->send()) {
                $ok = 1;
            }
        } catch (\EmailSendingFailedException $e) {
            \LogLib::warning($e, __METHOD__, $param);            
        } catch (\EmailValidationFailedException $e) {
            \LogLib::warning($e, __METHOD__, $param);            
        }
        \Model_Mail_Send_Log::add(array(
            'user_id' => !empty($param['user_id']) ? $param['user_id'] : 0,           
            'type' => \Model_Mail_Send_Log::TYPE_EMAIL_ADMIN_CREATE_USER,
            'title' => $subject,
            'content' => $body,
            'to_email' => $to,
            'status' => $ok
        ));
        return (boolean) $ok;
    }
     
    /**
     * Send register email
     *
     * @author thailh
     * @param array $param Information for sending email
     * @return bool Return true if successful ortherwise return false
     */
    public static function sendRegisterEmail($param) {
        if (self::beforeSend() == false) {
            return true;
        }
        return true;
        $subject = '会員登録頂きまして誠に有難うございました。';
    	$to = $param['email'];
    	$email = \Email::forge('jis');
    	$email->from(Config::get('system_email.noreply'), 'Bremen No reply');
    	$email->subject($subject);
    	$body = \View::forge('email/pc/register', $param);
    	$email->html_body($body);
    	$email->to(self::to($to));
        $ok = 0;
    	try {
    		\LogLib::info("Sent email to {$to}", __METHOD__, $param);
            if ($email->send()) {
                $ok = 1;
            }
    	} catch (\EmailSendingFailedException $e) {
    		\LogLib::warning($e, __METHOD__, $param);
    	} catch (\EmailValidationFailedException $e) {
    		\LogLib::warning($e, __METHOD__, $param);
    	}
        \Model_Mail_Send_Log::add(array(            
            'user_id' => !empty($param['user_id']) ? $param['user_id'] : 0,           
            'type' => \Model_Mail_Send_Log::TYPE_EMAIL_REGISTER,
            'title' => $subject,
            'content' => $body,
            'to_email' => $to,
            'status' => $ok
        ));
        return (boolean) $ok;
    }
    
     /**
     * Send when user quit
     *
     * @author Cao Dinh Tuan
     * @param array $param Information for sending email
     * @return bool Return true if successful ortherwise return false
     */
    public static function sendUserQuitEmail($param) {
        if (self::beforeSend() == false) {
            return true;
        }
        $subject = '退会処理が完了しました';
        $to = $param['email'];
        $email = \Email::forge('jis');
        $email->from(Config::get('system_email.noreply'), 'Bremen No reply');
        $email->subject($subject);
        $param['name'] = !empty($param['name']) ? $param['name'] : '';
        $body = \View::forge('email/pc/cancel_user', $param);
        $email->html_body($body);
        $email->to(self::to($to));
        $ok = 0;
        try {
            \LogLib::info("Sent email to {$to}", __METHOD__, $param);
            if ($email->send()) {
                $ok = 1;
            }
        } catch (\EmailSendingFailedException $e) {
            \LogLib::warning($e, __METHOD__, $param);
        } catch (\EmailValidationFailedException $e) {
            \LogLib::warning($e, __METHOD__, $param);
        }
        \Model_Mail_Send_Log::add(array(
            'user_id' => !empty($param['user_id']) ? $param['user_id'] : 0,
            'shop_id' => 0,
            'nailist_id' => 0,
            'type' => \Model_Mail_Send_Log::TYPE_EMAIL_QUIT_USER,
            'title' => $subject,
            'content' => $body,
            'to_email' => $to,
            'status' => $ok
        ));
        return (boolean) $ok;
    }
    
     /**
     * Send when user quite
     *
     * @author Cao Dinh Tuan
     * @param object $param Information for sending email
     * @return bool Return true if successful ortherwise return false
     */
    public static function resendEmail($param) {
        if (self::beforeSend() == false ) {
            return true;
        }
        if(empty($param->get('to_email'))){
             \LogLib::warning('Email is null or empty', __METHOD__, $param->get('to_email'));
            return false;
        }
        $to = $param->get('to_email');
        $email = \Email::forge('jis');
        $email->from(Config::get('system_email.noreply'), 'Bremen No reply');
        $email->subject( $param->get('title'));
        $body = $param->get('content');
        $email->html_body($body);

        $email->to(self::to($to));
        try {
            \LogLib::info("Resent email to {$to}", __METHOD__, $param);
            return $email->send();
        } catch (\EmailSendingFailedException $e) {
            \LogLib::warning($e, __METHOD__, $param);
            return false;
        } catch (\EmailValidationFailedException $e) {
            \LogLib::warning($e, __METHOD__, $param);
            return false;
        }
    }
    
    /**
     * Send test
     *
     * @author thailh
     * @param array $param Information for sending email
     * @return bool Return true if successful ortherwise return false
     */
    public static function sendTest($param) {
        if (self::beforeSend() == false ) {
            return true;
        }
        $to = !empty($param['to']) ? $param['to'] : '';
        if (empty($to)) {
            \LogLib::warning('Email is null or empty', __METHOD__, $to);
            return false;
        }
        $email = \Email::forge('jis');
        $email->from(Config::get('system_email.noreply'), '[Test] Bremen No reply');
        $email->subject('Test at ' . date('Y-m-d H:i'));
        $body  = 'This is message that sent from Bremen.town.<br/><br/>';       
        $email->html_body($body);
        $email->to(self::to($to));
        try {
            \LogLib::info("Resent email to {$to}", __METHOD__, $param);
            return $email->send();
        } catch (\EmailSendingFailedException $e) {
            \LogLib::warning($e, __METHOD__, $param);
            return false;
        } catch (\EmailValidationFailedException $e) {
            \LogLib::warning($e, __METHOD__, $param);
            return false;
        }
    }
}
