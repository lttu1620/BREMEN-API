<?php 
return array(	
	'img_dir' => realpath('../../' . dirname(basename(__FILE__))) . '/upload/img/',
	'path' => realpath('../../' . dirname(basename(__FILE__))) . '/upload/img/' . date('Y/m/d') . '/',
	'auto_process' => false,
	'normalize' => true,
	'change_case' => 'lower',
	'randomize' => true,	
	'ext_whitelist' => array('jpeg', 'jpg', 'gif', 'png', 'mp4', 'flv'),
    'max_size' => 1 * 1024 * 1024 // 1mb      
);